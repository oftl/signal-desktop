#!/bin/sh

source `dirname $0`/common.sh

podman run \
    --detach \
    --user signal \
    --name ${CONTAINER_NAME} \
    --label ${LABEL} \
    --rm \
    --interactive \
    --tty \
    --env DISPLAY \
    --network host \
    --volume ${VOLUME}:/home/signal/.config/Signal \
signal:latest
