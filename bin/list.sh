#!/bin/sh

source `dirname $0`/common.sh

podman ps --format "{{.Names}} | {{.Status}}" --filter label=${LABEL}
