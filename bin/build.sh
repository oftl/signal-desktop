#!/bin/sh

podman build \
    --force-rm \
    --no-cache \
    --tag signal \
    --file Containerfile \
.
