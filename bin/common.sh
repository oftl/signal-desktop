#!/bin/sh

export APP_NAME="${1:-$USER}"
export LABEL="signal"
export VOLUME="signal-${APP_NAME}"
export CONTAINER_NAME="signal-${APP_NAME}"
