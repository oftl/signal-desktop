Build and run to execute signal-desktop in a container. Then link your device as usual.
This way it is (also) possible to run multiple instances on one box.

 * common.sh`
     A couple of common env vars.

 * list.sh`
     List running instances.

 * `build.sh`
     Build an image (will be tagged `signal`) based on `Containerfile`

 * `run.sh [instance name]` run a one-off container off of the image `signal`
     Will create/use a named volume (defaults to just `signal`)

 * stop.sh`
     Stop it.
